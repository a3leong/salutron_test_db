<?php
/**
 * Created by IntelliJ IDEA.
 * User: remi
 * Date: 17/01/15
 * Modified by Aaron 6/25/15, 11:40
 * Time: 11:41
 */

$flattenDirectory = TRUE;

$base_directory = "uploads/";
$temp_directory = "temp/";
$temp_file = $temp_directory . basename($_FILES["upload_file"]["name"]);
$pathparts = pathinfo($temp_file);
$target_dir = $base_directory . $pathparts['filename'];   // target directory name strips extension

// Validate file extension, if not, output error msg
if($pathparts['extension'] !== 'rar' && $pathparts['extension'] !== 'zip')
{
	//echo 'Error, upload file must be archive file[1]';
	echo json_encode(array('scriptStatus'=>'-1'),JSON_NUMERIC_CHECK);
	exit;
}

// Upload the file
if (isset($_FILES['upload_file'])) {
    // Example:
    if(move_uploaded_file($_FILES['upload_file']['tmp_name'], $temp_file)){
        //echo $_FILES['upload_file']['name']. " upload ...";
    } else {
    	echo json_encode(array('scriptStatus'=>'2'),JSON_NUMERIC_CHECK);
    	exit;
        //echo $_FILES['upload_file']['name']. " NOT upload ..."; // TODO set to true or false echo for handling
    }
} else {
	echo json_encode(array('scriptStatus'=>'3'),JSON_NUMERIC_CHECK);
	exit;
    //echo "no";
}

// Now unpack the file based on zip or rar extension
if( strtolower($pathparts['extension']) === 'rar')
{ 
	// Php ver. 5.5.8 on EasyPHP, if RAR plugin is not working, probably using PHP 5.6 version
	$rar_file = RarArchive::open($temp_file);
	if($rar_file != FALSE) {      // not !== because success does not return bool type
		$list = $rar_file->getEntries();
		foreach($list as $entry) {
		    $entry->extract($target_dir); // extract to the current dir

		    if($flattenDirectory===TRUE && $entry->isDirectory()===TRUE)
		    {
		    	flattenDirectory($target_dir."/".($entry->getName())."/",$target_dir."/");
		    }
		}
		echo json_encode(array('scriptStatus'=>'1'),JSON_NUMERIC_CHECK);
		rar_close($rar_file);
		unlink($temp_file);
		exit;
	}
}
else if( strtolower($pathparts['extension']) === 'zip')
{
	$zip = new ZipArchive;
	if ($zip->open($temp_file) === TRUE) {
	    $zip->extractTo($target_dir);
	    $zip->close();
	    unlink($temp_file);                          // Delete temp file


	    // Cycle through all directories and extract folders
	    if( $flattenDirectory===TRUE) {
		    $files = scandir($target_dir);
		    $files = array_values( array_diff($files, array('..','.')) );   // Remove these useless listings that don't help
		    foreach($files as $f) {
		    	if( is_dir($target_dir."/".$f) ) {
		    		flattenDirectory($target_dir."/".$f."/", $target_dir."/");
		    	}
		    }
		}

	    echo json_encode(array('scriptStatus'=>'1'),JSON_NUMERIC_CHECK);
	    exit;
	    //echo 'File extract successful[zip]';
	} else {
    	//echo 'File extract failed[zip]';
    	echo json_encode(array('scriptStatus'=>'4'),JSON_NUMERIC_CHECK);
    	exit;
	}
}
else {
	echo json_encode(array('scriptStatus'=>'5'),JSON_NUMERIC_CHECK);
	exit;
}

/**
 * flattenDirectory()
 * Description: Takes moves all files in oldDirectory
 *              to newDirectory and deletes oldDirectory. 
 **/
function flattenDirectory($oldDirectory,$newDirectory)
{
	if($oldDirectory===$newDirectory)    // Should not be the same, error in php code if so
	{
		echo json_encode(array('scriptStatus'=>'6'),JSON_NUMERIC_CHECK);
		exit;               
	}
	// Get array of all source files
	$files = scandir($oldDirectory);
	// Identify directories
	$oldDirectory;
	$destination = $newDirectory;
	// Cycle through all source files
	foreach ($files as $file) {
		if (in_array($file, array(".",".."))) 
			continue;
		// If we copied this successfully, mark it for deletion
		if (copy($oldDirectory.$file, $destination.$file)) {
			$delete[] = $oldDirectory.$file;
		}
	}
	
	// Delete all successfully-copied files
	foreach ($delete as $file) {
		unlink($file);
	}
	rmdir($oldDirectory); // Remove the old directory
}