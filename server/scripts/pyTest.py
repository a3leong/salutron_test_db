# import cgitb #used for error testing


import os

from mod_python import apache #required

#Import required built in json library
try: 
  import simplejson as json
except:
  import json

'''
# index(req,postData)
# Description: Boilerplate code for Python script,
#              should not need to edit anything in this method.
#              Takes a list of file locations and changes current working directory (CWD)
#              and runs myMethod() when in each directory. 
# Return Value: Returns a JSON with output, errLog, and a list of files that return true
#               if myMethod() wants to set a condition.
#               output - Output accumulator string based on any feedback you want the file to return
#               errLog - errLog accumulator to handle errors and log them, will continue to next file, but
#                        return false for current file automatically, can either let the code catch errors automatically
#                        or throw custom errors.
#               resultFileList - Array to return that contains all filenames that return true for some comparison.
'''

def index(req,postData):
    try:
        # Function start code, should be consistent between all scripts.
        # Creates all necessary parameters
        jsonData = json.loads(postData)
        fileLocs = jsonData['fileLocs']
        directoryPath = __file__[:__file__.rfind('/')+1]              # Strip of filename and just get directory
        newPath = directoryPath+"../"
    except Exception, e:
        return json.dumps({"result":"", "err":e, "out":""})                       # Throw exception if the load is incorrect

    fileResultList = []               # String array accumulator containing files matching input
    errLog = ""                       # String accumulator for error log, build with errLog += "yourErrorHere\n"
    output = ""
    matchFlag = False                 # Flag to determine if file matches the query or not
    
    for filePath in fileLocs:         # Loop through every folder and perform a specified function on it
        try:
            filename = filePath[(filePath.rfind('/')+1):]      # Get filename
            os.chdir(newPath+filePath)        # Change directory to be in the current necessary directory TODO Fix
            methodReturn = myMethod()                   #    Calls your method on the current directory
            # Store tuple data into output stream and true/false conditional
            matchFlag = methodReturn[0]
            output = output + methodReturn[1] + "\n\n"
        except Exception, e:                      # If an error occurs, append error to errLog accumulator and skip to next file
            errLog += "Err in "+(''.join(filename))+":\n"+(str(e))+"\n"                     # Append error message to errLog accumulator                       
            continue
        else:                                     # No error raised by method, now compare result
            if matchFlag == True:                         # Add result to file list if the method returns true
                fileResultList.append(filename)
    return json.dumps({"result":fileResultList, "err":errLog, "out":output})       # Return a json dump 


'''
 # myMethod()
 # Definition: Your method goes here (keep same name). This is the method that is called after
 #             the working directory is changed to the current uploaded data folder. For example,
 #             you can open the MIO data file by looking for MIO.csv specifically without needing a filepath.
 #             If errors generated at runtime, the error is raised to be caught by the parent function automatically
 #             but can be thrown manually.
 #             
 #             Currently myMethod() simply just puts the heartrate data of a R500_HR.dat file into the output stream.
 #
 #             
 # Inputs: No inputs, can manually add here if you want.
 # Return Value:
 #     Returns a tuple that returns a boolean and an output stream. Return True for the 1st element
 #     of the tuple if you do not want to implement a comparison/bool logic check.
'''
def myMethod():
    try:
        boolCondition = True
        # Write your code here
        fo = open('R500_HR.dat','r') #open file as read only
        out = filename + ":\r\n"
        x = fo.readline()
        while( x != '' ):
            out = out+x
            x = fo.readline()
    except Exception, e:
        raise e
    else:
        #return a tuple from the method here 
        return (boolCondition,out)
