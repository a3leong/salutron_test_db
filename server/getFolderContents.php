<?php
error_reporting(0);    // Turn off error reporting so it doesn't mess with json data (need to remove the enabler later)
$directory = $_POST['folderLoc'];

$files = scandir($directory);
if($files === FALSE) {
	echo json_encode(array('scriptStatus'=>'1'),JSON_NUMERIC_CHECK);
}
else {
	// Remove '..' and '.' from array, array_values reindexes so you can iterate over the array in JS
	$files = array_values( array_diff($files, array('..','.')) );
	echo json_encode(array('files'=>$files));
}