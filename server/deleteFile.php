<?php
/*
 * Function taken from php website under rmdir documentation
 */
ignore_user_abort(true);
set_time_limit(0); // disable the time limit for this script

$path = $_POST['folderLoc']; // change the path to fit your websites document structure
function rrmdir($dir) { 
    if (is_dir($dir)) { 
        $objects = scandir($dir); 
        foreach ($objects as $object) { 
            if ($object != "." && $object != "..") { 
                if (filetype($dir."/".$object) == "dir")
                    rrmdir($dir."/".$object); 
                else
                    unlink($dir."/".$object); 
            } 
        } 
        reset($objects); 
        rmdir($dir); 
    } 
    else {
        $err = "ERROR: " . $dir . " not found";
        echo json_encode(array('scriptStatus'=>'0', 'err'=>$err),JSON_NUMERIC_CHECK);
        exit();
    }
} 

rrmdir($path); // Call the function
// Check to see if the folder still exists, failure if it still does
if( is_dir($path) ){
    $err = "ERROR: " . $path . " failed to delete";
    echo json_encode(array('scriptStatus'=>'0'),JSON_NUMERIC_CHECK);
}
else {
    echo json_encode(array('scriptStatus'=>'1'),JSON_NUMERIC_CHECK);
}